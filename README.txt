Hide This Term
=============

Description:
------------

Add option "Hide this term" to taxonomy term edit form.
If option checked, term will not visible on node view.


Dependencies:
----------------

Taxonomy Module


Author:
-------
Madhulata Infotech(Team). 
